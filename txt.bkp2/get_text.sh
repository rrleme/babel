#!/bin/bash

cont=8

while read -r linha
do
    echo $cont
    echo "$linha" > "2sec_${cont}_pt.tex"
    cont=$(( $cont + 1 ))
done < $1
