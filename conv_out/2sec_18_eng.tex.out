REF reported the existence of some phases for the adoption of virtual worlds by educators, synthesized below:

\begin{enumerate}

	\item \textbf{Pre-realisation:} no benefit is seen and/or would never use virtual worlds; have difficulty conceptualizing such use in the classroom and/or have conflicts with (or disagreeing with) their perception of ``real'' teaching and learning.

	\item \textbf{Realisation:} can see the benefit of virtual worlds, but are still unlikely to use them as they identify a number of barriers or concerns. They recognize personal feelings of discomfort or difficulty with virtual worlds, but feel that their students should have the opportunity to use such innovative technologies.

	\item \textbf{Replication:} make connections between virtual worlds and their current approach to the teaching-learning process, and demonstrate the desire to use them, no matter what barriers they may have perceived.

	\item \textbf{Implementation:} at this point, demonstrate a greater level of confidence with the use of virtual worlds and its application in the development of innovative teaching-learning processes.

	\item \textbf{Reimagining:} would not only use virtual worlds but could also describe their use as part of an innovative approach to the teaching-learning process.

\end{enumerate}

Thereby, this research also seeks to identify in which phase of adoption of virtual worlds the specialists are.
}
%pt57
