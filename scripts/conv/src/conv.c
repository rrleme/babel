#include<stdio.h>
#include<string.h>

int read_txt(char s[], FILE *p) {
  int n=0;
  char ch;
  while ((ch = getc(p)) != EOF) {
    s[n++] = ch;
  }
  s[n] = '\0';
  return n;
}

int insertID (char texto[], int ID[], int pos, int tam_texto) {
  int i = 0;
  int tam_ID=0;
  
  for (tam_ID = 0; ID[tam_ID] != 0; tam_ID++);
  for (i = tam_texto; i > pos; i--)
    texto[i+tam_ID-1] = texto[i];

  for (i = 0; i < tam_ID; i++)
    texto[pos+i] = 48+ID[i];

  return tam_ID;
}

void conv_string(char s[], int tam) {
  int i=0, top=0;
  int ID[100];
  
  for (i =0; i < 100; i++) ID[i] = 0;
  
  for (i = 0; i < tam; i++) {
    if (top == 0) ID[0] = 0; // comentar 
    if (s[i] == '#' && s[i+1] == '[') {
      // s[i+1]='&';
      ID[top] = ID[top]+1;
      top++;
      tam += insertID(s, ID, i, tam);
    }
    else if (s[i] == ']' && s[i+1] == '#') {
      //    s[i]='&';
      ID[top] = 0;
      top--;
      tam += insertID(s, ID, i+1, tam);
    }
    //printf("%c", s[i]);
  }

}

void main(int argc, char *argv[]) {
  FILE *p;
  char filename[100];
  int tam;
  char texto[999999];

  strcat(filename, argv[1]);
  p = fopen(filename, "r");
  tam = read_txt(texto, p);

  conv_string(texto, tam);

  strcat(filename, ".out");
  p = fopen(filename, "w");
  fprintf(p, "%s", texto);
}
