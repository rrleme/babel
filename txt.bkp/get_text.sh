#!/bin/bash

cont=8

while read -r linha
do
    echo $cont
    echo "$cont" > "2sec_${cont}_eng.tex"
    cont=$(( $cont + 1 ))
done < $1
